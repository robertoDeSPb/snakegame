// Fill out your copyright notice in the Description page of Project Settings.


#include "HighSpeedFood.h"
#include "PlayerPawnBase.h"
#include "SnakeBase.h"

// Sets default values
AHighSpeedFood::AHighSpeedFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AHighSpeedFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHighSpeedFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CurTime += DeltaTime;
	
	if(CurTime > 20.f)
	{
		this->Destroy();
		CurTime = 0.f;
	}
}


void AHighSpeedFood::SpawnFood()
{
	FRotator StartPointRotation = FRotator(0, 0, 0);

	float X = FMath::FRandRange(-500, 500);
	float Y = FMath::FRandRange(-500, 500);
	FVector NewLocation(X, Y, 0);

	GetWorld()->SpawnActor<AHighSpeedFood>(FoodActorClass, NewLocation, StartPointRotation);
}

void AHighSpeedFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->SetSpeed();
			this->Destroy();
		}
	}
}
