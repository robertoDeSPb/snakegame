// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "HighSpeedFood.generated.h"

class APlayerPawnBase;
class ASnakeBase;
UCLASS()
class SNAKEGAME1_API AHighSpeedFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHighSpeedFood();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<APlayerPawnBase> PlayerPawnClass;

	APlayerPawnBase* PlayerPawn;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AHighSpeedFood> FoodActorClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* interactor, bool bIsHead) override;

	void SpawnFood();

	float CurTime = 0.f;

};
