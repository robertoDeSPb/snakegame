// Fill out your copyright notice in the Description page of Project Settings.


#include "Wall.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
AWall::AWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWall::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			FVector NewLocation;
			FVector LastLocation = Snake->SnakeElements[0]->GetActorLocation();
		
			if (Snake->LastMoveDirection == EMovementDirection::UP)
			{
				//NewLocation.X = -980.f;
				//NewLocation.Y = 0;
				LastLocation.X = -960.f;
				LastLocation.Y = 0;
				Snake->SnakeElements[0]->AddActorWorldOffset(LastLocation);
			}
			else if (Snake->LastMoveDirection == EMovementDirection::DOWN)
			{
				//NewLocation.X = 980.f;
				//NewLocation.Y = 0;
				LastLocation.X = 960.f;
				LastLocation.Y = 0;
				Snake->SnakeElements[0]->AddActorWorldOffset(LastLocation);
			}
			else if (Snake->LastMoveDirection == EMovementDirection::RIGHT)
			{
				//NewLocation.X = 0;
				//NewLocation.Y = 980.f;
				LastLocation.X = 0;
				LastLocation.Y = 960.f;
				Snake->SnakeElements[0]->AddActorWorldOffset(LastLocation);
			}
			else if (Snake->LastMoveDirection == EMovementDirection::LEFT)
			{
				//NewLocation.X = 0;
				//NewLocation.Y = -980.f;
				LastLocation.X = 0;
				LastLocation.Y = -960.f;
				Snake->SnakeElements[0]->AddActorWorldOffset(LastLocation);
			}
			//Snake->SnakeElements[0]->AddActorWorldOffset(NewLocation);
		}
	}
}

