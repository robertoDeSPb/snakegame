// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;
class AFood;
class AHighSpeedFood;

UCLASS()
class SNAKEGAME1_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
		UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
		ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(BlueprintReadWrite)
		AFood* FoodActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodActorClass;

	UPROPERTY(BlueprintReadWrite)
		AHighSpeedFood* HighSpeedFoodActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AHighSpeedFood> HighSpeedFoodActorClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable, Category = "PlayerPawn")
	void CreateSnakeActor();

	UFUNCTION()
		void HandlePlayerVerticalInput(float value);
	UFUNCTION()
		void HandlePlayerHorizontalInput(float value);

	void SpawnFood();

	void SpawnHighSpeedFood();

	float StepDeley = 2.f;

	float BufferTime = 3;

	float CurTime = 0.f;

	//
	int32 GameMode = 0;

	UFUNCTION(BlueprintCallable, Category = "PlayerPawn")
	int32 GetGameMode() const {return GameMode;}

	UFUNCTION(BlueprintCallable, Category = "PlayerPawn")
	int32 GetScore();

	UPROPERTY(BlueprintReadWrite)
	bool GamePause = false;

	UFUNCTION(BlueprintCallable, Category = "PlayerPawn")
		bool GetGamePause() { return GamePause; }
};
